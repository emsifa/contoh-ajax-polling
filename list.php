<?php

session_start();

if(!isset($_SESSION['usernames'])) {
  $_SESSION['usernames'] = array();
}

$usernames = $_SESSION['usernames'];

?>

<html>
  <body>
    <h3>List Username</h3>
    <ul id="list">
      <?php 
        // tampilin username yang ada... 
        foreach($usernames as $username) {
          echo "<li>{$username}</li>";
        }
      ?>
    </ul>
    
    <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script>

      // ini untuk simpen username yang udah ditampilin...
      var list_username = <?php echo json_encode($usernames);?>;
      
      // fungsi poll, 
      function poll() {
        console.log("Polling...");

        $.getJSON('cek-update.php', function(usernames) {
          for(i in usernames) {
            var username = usernames[i];

            // kalo username belum di tampilin di list, tampilin!
            if($.inArray(username, list_username) < 0) {
              $("#list").append("<li>"+username+"</li>");
              list_username.push(username);
            }
          }
        }).always(function() {
          setTimeout(function() {
            poll();
          }, 1000); // tiap 1000ms nge-poll lagi...
        });
      }
            
      $(function() {
        poll(); // poll pertama kali
      });
    </script>
  </body>
</html> 