<?php

session_start();

if(!isset($_SESSION['usernames'])) {
  $_SESSION['usernames'] = array(); 
}

// file ini bakal diakses tiap 1000ms sekali untuk cek update...
// entah gimana caranya nanti, seharusnya data yang belum ditampilin aja di kirim...
// kalo disini kirim semuanya aja biar nggak ribet :v
exit(json_encode($_SESSION['usernames']));